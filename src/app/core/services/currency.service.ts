import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class CurrencyService {

  private BASE_URL: string = 'http://localhost:8080/api';

  constructor(private http: HttpClient) { }

  retrieveAllCurrencies(): Observable<any> {
    return this.http.get(`${this.BASE_URL}/currencies`);
  }

  convert(sourceCurrency: string, targetCurrency: string, amountToConvert: number): Observable<any> {
    return this.http.get(`${this.BASE_URL}/convert`, {
      params: { sourceCurrency, targetCurrency, amountToConvert }
    });
  }

}
