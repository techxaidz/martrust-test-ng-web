import { Component, OnInit } from '@angular/core';
import { CurrencyService } from './core/services/currency.service';
import { ConvertedCurrencyModel } from './models/converted-currency.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'martrust-test-ng-web';
  selectedSourceCurrency: string = 'EUR';
  selectedTargetCurrency: string = 'PHP';
  fromAmount: number = 1;
  toAmount: number = 0;
  baseRate: number = 0;
  currencies: string[] = [];

  constructor(private currencyService: CurrencyService) {

  }

  ngOnInit() {
    this.currencyService.retrieveAllCurrencies()
      .subscribe((data: string[]) => {
        this.currencies = data;
      });
    this.convertInitial();
  }

  convertInitial(): void {
    this.currencyService.convert(this.selectedSourceCurrency, this.selectedTargetCurrency, this.fromAmount)
      .subscribe((data: ConvertedCurrencyModel) => this.handleConversionResult(data));
  }

  convert(sourceCurrency: string, targetCurrency: string, amount: number): void {
    this.currencyService.convert(sourceCurrency, targetCurrency, amount)
      .subscribe((data: ConvertedCurrencyModel) => this.handleConversionResult(data));
  }

  handleConversionResult(data: ConvertedCurrencyModel): void {
    this.baseRate = data.baseRate;
    if (this.selectedSourceCurrency === data.sourceCurrency) {
      this.toAmount = data.convertedAmount;
    } else if (this.selectedTargetCurrency === data.targetCurrency) {
      this.fromAmount = data.convertedAmount;
    }
  }

}
