export interface ConvertedCurrencyModel {
  sourceCurrency: string;
  targetCurrency: string;
  baseRate: number;
  convertedAmount: number;
}
